func! myspacevim#before() abort
  set wrap
  augroup custom_autocmd
       au!
       au FileType text setlocal textwidth=78
  augroup END

  " doesn't work: https://github.com/SpaceVim/SpaceVim/issues/3527
  " call dein#add('junegunn/fzf', { 'build': './install --all', 'merged': 0 })
  " call dein#add('junegunn/fzf.vim', { 'depends': 'fzf' })
endf
func! myspacevim#after() abort
  set wrap
  au FileType markdown setlocal wrap
  au FileType markdown setlocal spell
endf
